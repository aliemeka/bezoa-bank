using System;
using System.Text;

namespace GrinBank.Model
{
    public class Account
    {
        public long Number{ get; set; }
        public decimal Balance { get; set; }
        public string Type { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
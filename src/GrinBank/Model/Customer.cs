using System;
using System.Collections.Generic;
using GrinBank.Model;

namespace GrinBank.Model
{
    public class Customer
    {
        public string AccountName;
        public int Age;
        public string Email;
        public string PhoneNumber;

        Account userAcct = new Account();
        
        public void setAccount(Account obj)
        {
            userAcct = obj;
        }
        
        public Customer(string firstname, string lastname, string email, string phonenumber, int age)
        {
            AccountName = firstname + " " + lastname;
            Email = email;
            PhoneNumber = phonenumber;
            Age = age;
        }
    }
}
namespace GrinBank.Service
{
    public interface IOperations
    {
         public void MakeTransactions();
         public void CreateNewAccount();
         public void GetAccountDetails();
         public void GetTransactionHistory();
         public void ChooseAction();
    }
}
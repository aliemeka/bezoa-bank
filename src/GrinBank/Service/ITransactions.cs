namespace GrinBank.Service
{
    /// <summary>
    ///  Interface for making difference bank transactions
    /// 
    /// </summary>
    public interface ITransactions
    {
         public void Deposit();
         public void Withdraw();
         public void CheckBalance();
         public void Transfer(int amount, string accountNumber);
    }
}
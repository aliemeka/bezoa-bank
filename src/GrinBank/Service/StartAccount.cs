using System;
using GrinBank.Model;

namespace GrinBank.Service
{
    public class StartAccount : IOpenAccount
    {
        public void OpenAccount()
        {
            string firstname;
            string lastname;
            string email;
            string phone;
            int age = 0;

            Console.WriteLine(@"Thank you for choosing Grin Bank. 
            Please enter your details below:");
            Console.Write("First name: ");
            firstname = Console.ReadLine();
            Console.Write("Last name: ");
            lastname = Console.ReadLine();
            Console.Write("Email: ");
            email = Console.ReadLine();
            Console.Write("Phone number: ");
            phone = Console.ReadLine();
            Console.Write("Age: ");
            Int32.TryParse(Console.ReadLine(), out age);

            Customer customer = new Customer(firstname, lastname, email, phone, age);

            Console.WriteLine($"Welcome {customer.AccountName}");

            Account account = new Account();
            Console.Write("Choose account type: 'S' : Savings or 'C' : Current -> ");
            string selection = Console.ReadLine();
            string accountType = ChooseAccountType(selection);
            Console.WriteLine($"You chose {accountType}");
            account.Type = accountType;
            
            account.Number = GenerateAccountNumber();
            account.Balance = 0;
            account.DateCreated = DateTime.Now;
            Console.WriteLine($"Your {account.Type} account with account number:{account.Number} has been created at {account.DateCreated.ToString("dd/MM/yyyy HH:mm")}");

            customer.setAccount(account);


        }

        private long GenerateAccountNumber()
        {
            System.Random random = new System.Random();
            return random.Next(299999999, 309999999);
        }

        private string ChooseAccountType(string type)
        {
            
            switch (type)
            {
                case "S":
                    return "Savings";
                case "C": 
                    return "Current";
                default:  
                    return "Savings";
            }
        }
    }
}
namespace GrinBank.Service
{
    public class Transactions : ITransactions
    {
        public void Deposit()
        {
            // Something happens
        }

        public void Withdraw()
        {
            // Something happens
        }

        public void CheckBalance()
        {
            // Something happens
        }

        public void Transfer(int amount, string accountNumber)
        {
            // Something happens
        }
    }
}